﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAmaralAssignment1
{
    public partial class Form1 : Form
    {
        public int turnCounter = 0;
        // Define possible turns
        private enum PlayerTurn { Player1, Player2 };
        // Game current turn
        private PlayerTurn playerTurn = PlayerTurn.Player1;
        // Players pick
        private Image player1Turn = Resources.Snake_X;
        private Image player2Turn = Resources.Snake_O;
        private int index;

        public Form1()
        {
            InitializeComponent();
        }

        //0 % 2 = 0 Player 1
        //1 % 2 = 1 Player 2
        //2 % 2 = 0 Player 1
        //3 % 2 = 1 Player 2
        private void pictureBox_Click(object sender, EventArgs e)
        {
            
            PictureBox picClicked = sender as PictureBox;
            groupBox1.Text = "";

            index = turnCounter % 2;

            if (index == 0)
            {
                picClicked.Image = player1Turn;
                picClicked.Enabled = false;
            }
            else if(index == 1)
            {
                picClicked.Image = player2Turn;
                picClicked.Enabled = false;
            }
            checkWinner();



            playerTurn = playerTurn == PlayerTurn.Player1 ? PlayerTurn.Player2 : PlayerTurn.Player1;
            turnCounter++; // Counter +1

        }

        // Players are both false = no one has won
        // If 3 images are  the same depending if all images counts are an index of 0(player1) or 1(player2)

        private void checkWinner()
        {
            bool playerOne = false;
            bool playerTwo = false;
            if (!pictureBox1.Enabled)
            {
                
                if ((pictureBox1.Image == pictureBox2.Image) && (pictureBox2.Image == pictureBox3.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;

                    }
                    else if (index == 1)
                    {
                        playerTwo = true;

                    }


                }

            }
            if (!pictureBox4.Enabled)
            {

                if ((pictureBox4.Image == pictureBox5.Image) && (pictureBox5.Image == pictureBox6.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }


                }

            }

            if (!pictureBox7.Enabled)
            {

                if ((pictureBox7.Image == pictureBox8.Image) && (pictureBox8.Image == pictureBox9.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }

            }

            if (!pictureBox1.Enabled)
            {

                if ((pictureBox1.Image == pictureBox4.Image) && (pictureBox4.Image == pictureBox7.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }

            }

            if (!pictureBox2.Enabled)
            {

                if ((pictureBox2.Image == pictureBox5.Image) && (pictureBox5.Image == pictureBox8.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }

            }

            if (!pictureBox3.Enabled)
            {

                if ((pictureBox3.Image == pictureBox6.Image) && (pictureBox6.Image == pictureBox9.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }

            }

            if (!pictureBox1.Enabled)
            {

                if ((pictureBox1.Image == pictureBox5.Image) && (pictureBox5.Image == pictureBox9.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }

            }

            if (!pictureBox3.Enabled)
            {

                if ((pictureBox3.Image == pictureBox5.Image) && (pictureBox5.Image == pictureBox7.Image))
                {
                    if (index == 0)
                    {
                        playerOne = true;
                    }
                    else if (index == 1)
                    {
                        playerTwo = true;
                    }

                }
            }
            if (playerOne == true)
            {
                MessageBox.Show("player one wins");
                reset();
            }
            else if (playerTwo == true)
            {
                MessageBox.Show("player two wins");
                reset();
            }
            if (turnCounter == 8 && !playerOne && !playerTwo)
            {
                MessageBox.Show("It's a tie!");
                reset();
            }
            

        }

        //Enable images
        //Empty images
        //Reset counter
        private void reset()
        {

            pictureBox1.Enabled = true;
            pictureBox2.Enabled = true;
            pictureBox3.Enabled = true;
            pictureBox4.Enabled = true;
            pictureBox5.Enabled = true;
            pictureBox6.Enabled = true;
            pictureBox7.Enabled = true;
            pictureBox8.Enabled = true;
            pictureBox9.Enabled = true;

            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox6.Image = null;
            pictureBox7.Image = null;
            pictureBox8.Image = null;
            pictureBox9.Image = null;

            turnCounter = 0;
        }

    }
}
