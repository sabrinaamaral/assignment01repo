
<h1> To install the build, first you must download the repository.</h1>

**Once the repository is downloaded, unzip the folder to any location you desire.**


	           
![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/zipped.jpg)**Zipped**
![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/unzipped.jpg) **Unzipped**




**This README.md will be located before the build folder for your viewing.**

![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/location.jpg)
 
**Click onto the SAmaralAssignment1 folder and double click the sln file named SAmaralAssignment1.sln that is inside that SAmaralAssignment1 folder**
 

![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/solution.jpg)

**Once the application has been opened, on the right-hand side you will see the SAmaralAssignment1. Right click on SAmaralAssignment1 and build the project. Please see gif below for reference.** 

![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/build.gif)

**Once the project is built, you can go ahead and double click the form1.cs and begin to play a game of TicTocToe by clicking start.**

![](https://bitbucket.org/sabrinaamaral/assignment01repo/raw/d2718714c5d1d96a199f83f20790e874658c04e2/images/layout.jpg)

**X(player1) goes first, O(player2) goes second. Whoever gets 3 of their match images horizontally, vertically, or diagonally first win!**

---
**NOTE:**

Reasoning for why I picked my MIT License for my project:

The reason I picked a MIT License for my project is because I would like to see others contribute to my open source code.	
The MIT License grants permission for developers to allow free distribution, modification, and free use while still having the project licensed.

---